"""
Author: Korbzen
Date: 2019-12-16

This file contains the skeleton class for all existing plots.
"""

import matplotlib.pyplot as plt

class masterplot():
    """Framework Skeleton-Class
    
    This class implements the interfaces for all available plots.
    
    Attributes
    ---------
    colors: dict-like
        the pre-configured set of colors
    
    Methods
    --------
    plot(x,y,z): Plotting function
        This function draws the plot with given data x,y,z.
    
    """
    colors = dict(accents = ['#000000', '#FFFFFF'],
                  colors  = ['#000000', '#FFFFFF'])
    
    def __init__(self):
        pass
    
    def plot(self):
        pass
    
    """
    Different idea:
        make all plots functions and create a object that's a singleton and holds all the configuration
        
        pro:
            multiple plots with a single config-object that one change 
            during run-time changes all the others as well
    """